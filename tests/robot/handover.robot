*** Settings ***
Documentation     Validate the Handover page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout


*** Test Cases ***
Perform Handover
    Login To User Not Currently On Pager
    Open Handover Dialog
    Handover Dialog Should Be Open
    Input Message
    Take Over Pager
    Handover Page Should Be Open
    Pager Has Been Taken
    Go To Home Page
    Current User Has Pager


*** Keywords ***
Login To User Not Currently On Pager
    ${test1_has_pager}=    Run Keyword And Return Status    Check If test1 Has Pager
    Run Keyword If    $test1_has_pager == False    Logout
    Run Keyword If    $test1_has_pager == False    Login To test1
    Run Keyword If    $test1_has_pager == True     Login To test2
    Home Page Should Be Open


Check If test1 Has Pager
    Login To test1
    Current User Has Pager
    Logout

Input Message
    Input Text    tater-message    eu6tahngi7oo0chu6ohmopa4aecheyeKei9ohngul1eetuGh

Take Over Pager
    Click Button    id: take-tater-submit-button

Current User Has Pager
    Page Should Contain    You hold the tater

Pager Has Been Taken
    Page Should Contain    Pager taken!
