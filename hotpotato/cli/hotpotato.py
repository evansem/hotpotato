"""
Command line interface (CLI) Hot Potato commands.
"""


import json

import click
import flask

from hotpotato import __version__


@click.group("hotpotato", cls=flask.cli.AppGroup)
def hotpotato():
    """
    Hot Potato admin commands.
    """

    pass


@hotpotato.command("config")
def config():
    """
    Hot Potato configuration.
    """

    click.echo(
        json.dumps(
            {k: str(v) for k, v in flask.current_app.config.items()},
            indent=2,
            sort_keys=True,
        )
    )


@hotpotato.command("version")
def version():
    """
    Hot Potato version.
    """

    click.echo(__version__)
