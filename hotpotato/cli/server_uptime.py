"""
Command line interface (CLI) server uptime functions.
"""


import json

import click
import click_datetime
import flask.cli

from hotpotato import server_uptimes


@click.group("server-uptime", cls=flask.cli.AppGroup)
def server_uptime():
    """
    Server uptime statistics commands.
    """

    pass


@server_uptime.command("update-avail-last-1d-7d")
def update_avail_last_1d_7d():
    """
    Update the table for the last day and week.
    """

    server_uptimes.update_avail_last_1d_7d()


@server_uptime.command("update-avail-last-month")
def update_avail_last_month():
    """
    Update the table for the previous month.
    """

    server_uptimes.update_avail_last_month()


@server_uptime.command("get-avail-between")
@click.argument("start_date", type=click_datetime.Datetime(format="%Y-%m-%d"))
@click.argument("end_date", type=click_datetime.Datetime(format="%Y-%m-%d"))
@click.option(
    "--server-id",
    type=click.INT,
    default=None,
    help="ID of server to get statistics for.",
)
def get_avail_between(start_date, end_date, server_id):
    """
    Get availability statistics between two dates.
    """

    avails = server_uptimes.get_avail_between(start_date, end_date, server_id=server_id)

    if server_id:
        results = {server_id: avails}
    else:
        results = {si: a for si, a in avails}

    click.echo(json.dumps(results, indent=4))
