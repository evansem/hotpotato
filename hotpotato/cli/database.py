"""
Command line interface to create database.
"""


import click
import flask
import flask.cli

from hotpotato import models, roles, rotations, users


@click.group("database", cls=flask.cli.AppGroup)
def database():
    """
    Database commands.
    """

    pass


@database.command("create")
def create():
    """
    Create the database
    """
    models.create(flask.current_app)
    models.initialise(flask.current_app)

    with flask.current_app.app_context():
        rotations.create("sysadmins", None)


@database.command("create-dev")
def create_dev():
    """
    Create the database, with test users set up
    """
    models.create(flask.current_app)
    models.initialise(flask.current_app)

    with flask.current_app.app_context():
        user_test1 = users.create(
            "test1@example.com", "test_password1", "Test User 1", "UTC"
        )
        user_test2 = users.create(
            "test2@example.com", "test_password2", "Test User 2", "UTC"
        )

        role_admin = roles.get_by_name("admin")
        user_test1.add_role(role_admin)
        user_test2.add_role(role_admin)
        models.db.session.commit()

        rotations.create("sysadmins", user_test2.id)
