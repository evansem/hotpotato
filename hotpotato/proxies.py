"""
Proxy functions for data stored locally and in the database
"""

from flask import g

from hotpotato.models import Rotation, User

DEFAULT_TZ = "UTC"


def get_current_pager_person(rotation):
    """
    Get the current pager person for the rotation. Returns None if not found.
    """
    if "pager_person" not in g:
        g.pager_person = {}
    if rotation not in g.pager_person:
        currently_oncall = Rotation.query.filter_by(rotname="sysadmins").one_or_none()
        if currently_oncall is None:
            g.pager_person[rotation] = None
        else:
            g.pager_person[rotation] = User.query.filter_by(
                id=currently_oncall.person
            ).one_or_none()
    return g.pager_person[rotation]
