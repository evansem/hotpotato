"""
OncallContact model classes.
"""
from hotpotato import notifications as hotpotato_notifications
from hotpotato.models.database import db
from hotpotato.models.users import User


class OncallContact(db.Model):
    """
    Stores contact details for on-call people.
    Stores arbitrary strings such as phone numbers, user names, etc.
    """

    __tablename__ = "oncall_contacts"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey(User.id, ondelete="CASCADE"), nullable=False
    )
    user = db.relationship(User, uselist=False)
    method = db.Column(db.Enum(*hotpotato_notifications.METHOD), nullable=False)
    # Verifiability can also be automatically determined from the method type,
    # but for data query and priority queuing purposes, it is also defined in the database.
    # FIXME: Should be nullable=False but CockroachDB doesn't allow the constraint to be
    #        added through ALTER TABLE ADD CONSTRAINT at this time.
    verifiable = db.Column(db.Boolean)
    # Priority works in ascending order (e.g. 0 is the highest priority).
    # FIXME: Should be nullable=False but CockroachDB doesn't allow the constraint to be
    #        added through ALTER TABLE ADD CONSTRAINT at this time.
    priority = db.Column(db.Integer)
    contact = db.Column(db.Text, nullable=False)
    send_pages = db.Column(db.Boolean, nullable=False)
    send_failures = db.Column(db.Boolean, nullable=False)
