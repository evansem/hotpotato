from hotpotato.models.notifications.notifications import Notification


class Handover(Notification):
    """
    Handover notification object class.
    """

    name = "handover"
    description = "handover notification"

    # pylint: disable=arguments-differ
    def send(self, run_async=True, contact=None, method=None):
        """
        Send the handover.
        """

        # The reason why this import is here and not at the top of the file is
        # because when the actors get defined, they connect to the message queue broker
        # to register themselves as jobs to be run.
        # Therefore, the message queue broker needs to already be running BEFORE
        # the actors get imported/defined.
        from hotpotato.notifications.queue.actors import handover as actor

        super().send(actor, run_async=run_async, contact=contact, method=method)

    __mapper_args__ = {"polymorphic_identity": "handover"}
