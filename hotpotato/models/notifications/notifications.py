import json

import sqlalchemy.ext.mutable
from sqlalchemy.orm import validates

from hotpotato import notifications, util
from hotpotato.models.database import db
from hotpotato.models.users import User
from hotpotato.notifications import exceptions


class Notification(db.Model):
    """
    Stores a log of notifications sent through and received by Hot Potato.
    """

    __tablename__ = "notification_log"

    id = db.Column(db.Integer, primary_key=True)
    tenant_id = db.Column(db.Integer)
    user = db.relationship(User, uselist=False)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id, ondelete="SET NULL"))
    received_dt = db.Column(db.DateTime, nullable=False)
    notif_type = db.Column(db.Enum(*notifications.TYPE), nullable=False)
    body = db.Column(db.Text, nullable=False)
    # The built-in JSON types do not detect in-place modifications, which are used
    # extensively. In order for this to work, the MutableDict type, part of
    # the sqlalchemy.ext.mutable module, is required.
    json = db.Column(
        sqlalchemy.ext.mutable.MutableDict.as_mutable(db.JSON), nullable=False
    )

    __mapper_args__ = {
        "polymorphic_on": notif_type,
        "polymorphic_identity": "notification",
    }

    """
    Notification object class.
    """

    name = "notification"
    description = "notification"

    @validates("json")
    def validate_json(self, key, json_data):
        """
        Validate the json data.
        """

        if json_data["status"] not in notifications.STATUS:
            raise exceptions.NotificationStatusError(
                json_data["status"],
                notif_id=self.id,
                possible_values=notifications.STATUS,
            )

        if json_data["method"] and json_data["method"] not in notifications.METHOD:
            raise exceptions.NotificationMethodError(
                json_data["method"], notif_id=self.id
            )

        if (
            json_data["provider"]
            and json_data["provider"] not in notifications.PROVIDER
        ):
            raise exceptions.NotificationProviderError(
                json_data["provider"], notif_id=self.id
            )

        if json_data["provider_notif_id"] and not isinstance(
            json_data["provider_notif_id"], str
        ):
            raise exceptions.NotificationProviderNotificationIDTypeError(
                json_data["provider_notif_id"], notif_id=self.id
            )

        return json_data

    def received_dt_get_as_string(self):
        """
        Return this notification's received_dt field as a string in ISO 8601 format.
        """

        return util.datetime_get_as_string(self.received_dt)

    def received_dt_set_from_string(self, datetime_string):
        """
        Set this notification's received_dt field to the given string in ISO 8601 format.
        """

        self.received_dt = util.datetime_get_from_string(datetime_string)

    def as_json(self, **kwargs):
        """
        Express this notification as a JSON string.
        """

        return json.dumps(
            {
                "id": self.id,
                "tenant_id": self.tenant_id,
                "received_dt": self.received_dt_get_as_string(),
                "user_id": self.user_id,
                "notif_type": self.notif_type,
                "body": self.body,
                "json": self.json,
            },
            **kwargs
        )

    def is_unsent(self):
        """
        Returns True if this notification has not yet been sent.
        """

        return self.json["status"] in ("NEW", "UNSENT")

    def failed_to_send(self):
        """
        Returns True if this notification failed to send.
        """

        return self.json["status"] in ("SEND_FAILED", "SEND_FAILED_ACKNOWLEDGED")

    def is_sending(self):
        """
        Returns True if this notification has been sent, or is currently sending.
        """

        return True if self.json["status"] == "SENDING" else self.has_been_sent()

    def has_been_sent(self):
        """
        Returns True if this notification has been sent to its contact method provider.
        """

        return self.json["status"] in (
            "SENT_TO_PROVIDER",
            "RECEIVED_BY_PROVIDER",
            "SENT_TO_CLIENT",
            "RECEIVED_BY_CLIENT",
            "READ_BY_CLIENT",
        )

    def has_been_received_by_client(self):
        """
        Returns True if this notification has been received by the client.
        """

        return self.json["status"] in ("RECEIVED_BY_CLIENT", "READ_BY_CLIENT")

    def send(self, actor, run_async=True, contact=None, method=None):
        """
        Send the notification.
        """

        contact_id = contact.id if contact else None

        if self.is_sending():
            return
        if run_async:
            actor.send(self.id, contact_id=contact_id, method=method)
        else:
            actor(self.id, contact_id=contact_id, method=method)
