import pytest

from hotpotato import roles as role_functions


def test_get_by_name_role(app, db):
    """
    test that the get by name works
    """
    with app.app_context():
        wrong_name = "fail"
        correct_name = "admin"
        role = role_functions.get_by_name(correct_name)
        assert role is not None
        with pytest.raises(role_functions.RoleNameError):
            role_functions.get_by_name(wrong_name)


def test_get(app, db):
    """
    test that get returns something
    """
    with app.app_context():
        wrong_name = "fail"
        correct_name = "admin"
        role = role_functions.get_by_name(correct_name)
        role_id = role.id
        assert role_functions.get(role_id)
        assert role_id is not None
        assert role.name == correct_name
        with pytest.raises(role_functions.RoleNameError):
            role_functions.get_by_name(wrong_name)


def test_get_by(app, db):
    """
    check that the get_by function returns something
    """
    with app.app_context():
        roles = role_functions.get_by(name="*")
        assert roles is not None


def test_create(app, db):
    with app.app_context():
        role = role_functions.create("examp", "example user")
        assert role == role_functions.get_by_name("examp")
