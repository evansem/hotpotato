import factory

from hotpotato.models import Rotation, db
from hotpotato.tests import users


class RotationFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Rotation
        sqlalchemy_session = db.session

    rotname = "sysadmins"
    user = factory.SubFactory(users.UserFactory)
