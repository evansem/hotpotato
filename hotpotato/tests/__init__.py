"""
Hot Potato testing module.
"""


import faker
import sqlalchemy.exc

from hotpotato.models import db
from hotpotato.tests import (
    heartbeats,
    notifications,
    oncall_contacts,
    server_uptimes,
    servers,
    users,
)


def create_data(num_servers=3, num=100, seed=None):
    """
    Creates some test data for testing purposes.
    This tries quite hard to be generic across the database model.
    """

    fake = faker.Faker()
    if seed:
        fake.seed(seed)

    # Users
    print("Creating users... ", end="", flush=True)
    for _ in range(num):
        user = users.create_fake(fake)
        if user:
            try:
                db.session.add(user)
                db.session.commit()
            except sqlalchemy.exc.InvalidRequestError:
                print("caught InvalidRequestError")
                continue
    print("done")

    # On-call contacts
    print("Creating on-call contacts... ", end="", flush=True)
    for _ in range(num):
        onc = oncall_contacts.create_fake(fake)
        if onc:
            db.session.add(onc)
    db.session.commit()
    print("done")

    # Servers
    print("Creating servers, server uptimes and heartbeats... ", end="", flush=True)
    for _ in range(num_servers):
        server = servers.create_fake(fake)
        if not server:
            continue
        db.session.add(server)
        db.session.commit()
        db.session.refresh(server)
        if server:
            db.session.add(server_uptimes.create_fake(fake, server))
            for hbeat in heartbeats.create_fakes(fake, server):
                db.session.add(hbeat)
            db.session.commit()
    print("done")

    # Notifications
    print("Creating notifications... ", end="", flush=True)
    for _ in range(num):
        notif = notifications.create_fake(fake)
        if notif:
            db.session.add(notif)
    db.session.commit()
    print("done")
