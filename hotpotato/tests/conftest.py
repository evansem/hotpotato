"""
Provides fixtures for all tests.
"""


# pylint: disable=redefined-outer-name


import contextlib
import json
import logging

import pytest

from hotpotato import app as hotpotato_app, models, users
from hotpotato.tests import (
    notifications,
    oncall_contacts,
    rotations,
    servers,
    users as test_users,
)


@pytest.fixture
def logger():
    """
    Returns pytest's logger. Only warning & above messages will be displayed.
    """

    yield logging.getLogger(__name__)


@pytest.fixture(scope="session")
def app():
    """
    Create the app with test-specific settings.
    """

    # Designed to be run in the Docker image.
    preset = {
        "flask": {"testing": "true"},
        "modica": {"enabled": "true"},
        "twilio": {"enabled": "true"},
        "app": {"enabled": "true"},
        "pushover": {"enabled": "true"},
        "pager": {"enabled": "true"},
        "sms": {"enabled": "true"},
    }

    _app = hotpotato_app.create(config_preset=preset, config_use_defaults=False)

    _app.config["WTF_CSRF_ENABLED"] = False

    _app.config["SQLALCHEMY_DATABASE_URI"] = "cockroachdb://{}:{}/{}test".format(
        _app.config["COCKROACH_SERVER"],
        _app.config["COCKROACH_PORT"],
        _app.config["COCKROACH_DATABASE"],
    )

    yield _app


@pytest.fixture
def client(app):
    """
    Return a test client.
    """

    _client = app.test_client()
    yield _client


@pytest.fixture
def runner(app):
    """
    Return a cli runner
    """

    yield app.test_cli_runner()


@pytest.fixture(scope="session")
def db(app):
    """
    Return the test database. Handles setup & teardown.
    """

    with app.app_context():

        # Create the database if it doesn't exist.
        models.create(app)

        # Create the tables and initialise them with test data.
        models.reinitialise(app)

        yield models.db

        # Drop all the tables in the database.
        models.db.drop_all()


@pytest.fixture(scope="function")
def session(db):
    """
    Return a session on the test database. Wraps everything in
    a transaction and rolls it back on teardown.
    """
    connection = db.engine.connect()
    transaction = connection.begin()
    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)
    db.session = session
    test_users.UserFactory._meta.sqlalchemy_session = session
    rotations.RotationFactory._meta.sqlalchemy_session = session
    servers.ServerFactory._meta.sqlalchemy_session = session
    notifications.NotificationFactory._meta.sqlalchemy_session = session
    oncall_contacts.OncallContactsFactory._meta.sqlalchemy_session = session

    yield session

    transaction.rollback()
    connection.close()
    session.remove()


@pytest.fixture
def make_user(session):
    """
    Factory for creating users with a particular role.
    """

    # Ensure we create unique email addresses
    n = 1

    def _create_user(rolename):
        nonlocal n
        new_user = users.create(
            "{}@example.com".format(n), "password", "Test user", "NZ"
        )
        role = models.user_datastore.find_role(rolename)
        new_user.roles.append(role)
        session.add(new_user)
        session.commit()

        n += 1

        return new_user

    yield _create_user


@contextlib.contextmanager
def login(client, user):
    """
    Minimal context manager for performing requests with a logged in user.
    """

    r = client.post(
        "/login",
        data=json.dumps(dict(email=user.email, password="password", csrf_token="")),
        content_type="application/json",
    )
    assert r.status_code == 200

    yield user

    r = client.post("/logout", follow_redirects=True)
