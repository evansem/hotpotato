"""
Test server related views
"""

from bs4 import BeautifulSoup

from hotpotato.servers import get_by_api_key
from hotpotato.tests import users
from ..conftest import login


def test_list(app, client, session):
    """
    Test that the server list page renders.
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get("/servers", follow_redirects=True)
        assert r.status_code == 200


def test_create(app, client, session):
    """
    Test that a server can be greated, and that the API key returned is correct.
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            "/servers/add",
            data=dict(
                hostname="testhost", timezone="NZ", link="", missed_heartbeat_limit=0
            ),
            follow_redirects=True,
        )

        assert r.status_code == 200

        soup = BeautifulSoup(r.data, "html.parser")

        assert get_by_api_key(soup.pre.string).hostname == "testhost"
