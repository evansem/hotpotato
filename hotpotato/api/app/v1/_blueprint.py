"""
App API version 1 blueprint.
"""


import flask

blueprint = flask.Blueprint("api_app_v1", __name__)
