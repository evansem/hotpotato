"""
Server API version 1 endpoint for adding alerts.
"""


from http import HTTPStatus

import flask
from flask import current_app
from marshmallow import Schema, fields

from hotpotato import rotations, servers, users, util
from hotpotato.api import functions as api_functions
from hotpotato.api.functions import exceptions as api_functions_exceptions
from hotpotato.api.server.v1._blueprint import blueprint
from hotpotato.notifications import alerts


class APIDateTime(fields.Field):
    """
    Field that deserializes from old not fully iso8601 compliant hot potato timestamps
    """

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return ""
        return util.datetime_get_as_string(value)

    def _deserialize(self, value, attr, data, **kwargs):
        return util.datetime_get_from_string(value, tz_space_sep=True)


class AlertDataSchema(Schema):
    apikey = fields.Str()
    id = fields.Int()
    hostname = fields.Str()
    service_name = fields.Str(load_from="servicename")
    trouble_code = fields.Str(load_from="troublecode")
    display_name = fields.Str(load_from="displayname")
    output = fields.Str()
    state = fields.Str()
    timestamp = APIDateTime(load_from="icingadatetime")

    class Meta:
        strict = True


# pylint: disable=inconsistent-return-statements
@blueprint.route("/add/notification", methods=["POST"])
def add_notification():
    """
    Create an alert, and send it to the on-call person.
    """

    data = api_functions.data_get(schema=AlertDataSchema())

    try:
        server = servers.get_by_api_key(data["apikey"])
        # TODO: changeable rotation
        user = users.get_from_rotation(rotations.get_by_name("sysadmins"))

        current_app.logger.debug(
            "The person currently on-call is: {0} ({1})".format(user.name, user.id)
        )

        # Generate the alert and put it on the message queue.
        ale = alerts.create(
            None,  # server.tenant_id, # TODO: tenants
            user_id=user.id,
            alert_type="service"
            if "service_name" in data and data["service_name"]
            else "host",
            server=server,
            trouble_code=data["trouble_code"],
            hostname=data["hostname"],
            display_name=data["display_name"],
            service_name=data["service_name"] if "service_name" in data else None,
            state=data["state"],
            output=data["output"],
            timestamp=util.datetime_process(
                data["timestamp"],
                default_tz=server.timezone_get_as_timezone(),
                to_utc=True,
                naive=True,
            ),
        )
        ale.send()

        return flask.make_response(str(ale.id), HTTPStatus.CREATED)

    except api_functions_exceptions.APIDataValueError as err:
        return flask.make_response(str(err), HTTPStatus.BAD_REQUEST)

    except servers.ServerAPIKeyError as err:
        return flask.make_response(str(err), HTTPStatus.FORBIDDEN)
    except KeyError as err:
        return flask.make_response(str(err), HTTPStatus.BAD_REQUEST)
