"""
Server API version 2 sender endpoints.
"""


from hotpotato.api.server.v2.senders import modica  # noqa: F401
from hotpotato.api.server.v2.senders import pushover  # noqa: F401
from hotpotato.api.server.v2.senders import twilio  # noqa: F401
