"""
Server API version 2 notification handler.
"""


from marshmallow import Schema, fields

from hotpotato.notifications.notifications import NOTIF_TYPE, get

__all__ = ["NOTIF_TYPE", "get", "schema"]


class NotificationSchema(Schema):
    id = fields.Int(required=True)
    tenant_id = fields.Int(required=True)
    user_id = fields.Int(required=True)
    received_dt = fields.DateTime(required=True, format="iso")
    notif_type = fields.Str(required=True)
    body = fields.Str(required=True)
    version = fields.Int(required=True, json=True)
    node_name = fields.Str(required=True, json=True)
    event_id = fields.Int(required=True, json=True)
    ticket_id = fields.Int(required=True, json=True)
    warnings = fields.List(fields.Str(), json=True)
    errors = fields.List(fields.Str(), required=True, json=True)
    status = fields.Str(required=True, json=True)
    method = fields.Str(required=True, json=True)
    provider = fields.Str(required=True, json=True)
    provider_notif_id = fields.Str(required=True, json=True)

    def get_attribute(self, attr, obj, default):
        if "json" in self.fields[attr].metadata and self.fields[attr].metadata["json"]:
            return super().get_attribute("json.{}".format(attr), obj, default)
        return super().get_attribute(attr, obj, default)

    class Meta:
        strict = True


schema = NotificationSchema()
