"""
Server API key functions.
"""


import random
import string


def create():
    """
    Create an API key for a server.
    Currently, this generates a 100 character alphanumeric string.
    """

    api_key = "".join(
        random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
        for _ in range(100)
    )

    return api_key
