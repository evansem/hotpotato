"""
API helper functions.
"""


import csv
import io
from http import HTTPStatus

import flask
import werkzeug.datastructures
from webargs import flaskparser

from hotpotato.api.functions import exceptions


def method_get():
    """
    Return the method type of the current request.
    """

    return flask.request.method


def args_get(silent=False):
    """
    Return an ImmutableMultiDict of API URL argument data from a GET, POST or PUT request.
    """

    if flask.request.args:
        return flask.request.args
    elif silent:
        return None
    else:
        raise exceptions.APIDataValueError(
            "Request is not a data-encoded URL",
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )


_parser = flaskparser.FlaskParser()


def data_get(
    allow_json=True, allow_form=True, allow_args=True, silent=False, schema=None
):
    """
    Return an ImmutableMultiDict of API GET/POST data from a request.
    This tries to automatically detect the type of data.
    """

    if schema:
        locations = []
        if allow_json:
            locations.append("json")
        if allow_form:
            locations.append("form")
        if allow_args:
            locations.append("query")
        return werkzeug.datastructures.ImmutableMultiDict(
            mapping=_parser.parse(schema, flask.request, locations=locations).items()
        )

    if allow_json and flask.request.is_json:
        return werkzeug.datastructures.ImmutableMultiDict(
            mapping=flask.request.get_json().items()
        )
    elif allow_form and flask.request.form:
        return flask.request.form
    elif allow_args and flask.request.args:
        return flask.request.args
    elif silent:
        return None
    else:
        raise exceptions.APIDataValueError(
            "Unable to find user data in request",
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )


@_parser.error_handler
def handle_error(error, req, schema):
    raise exceptions.APIDataUnprocessableError(
        "Request is not valid JSON for this endpoint", error
    )


def json_response_get(json_obj, code=200, no_cache=False):
    """
    Return a JSON request response.
    """

    response = flask.make_response(flask.jsonify(json_obj), code)

    if no_cache:
        # For HTTP/1.1 clients
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        # For HTTP/1.0 clients (curl)
        response.headers["Pragma"] = "no-cache"
        # For HTTP/1.0 proxies
        response.headers["Expires"] = "0"

    return response


def csv_response_get(columns, rows, code=200):
    """
    Return a CSV request response.
    """

    string_io = io.StringIO()
    csv_writer = csv.writer(string_io)

    csv_writer.writerow(columns)
    for row in rows:
        csv_writer.writerow(row)

    response = flask.make_response(string_io.getvalue(), code)
    response.headers["Content-Disposition"] = "attachment; filename=export.csv"
    response.headers["Content-Type"] = "text/csv"

    return response
