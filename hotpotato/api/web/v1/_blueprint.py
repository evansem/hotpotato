"""
Web API version 1 blueprint.
"""


import flask

blueprint = flask.Blueprint("api_web_v1", __name__)
