"""
Heartbeat functions.
"""


from datetime import date, datetime

import sqlalchemy

from hotpotato.models import Heartbeat as Model, db


def create(server_id):
    """
    Create a new heartbeat for the server with the given ID.
    """

    heartbeat = Model(server_id=server_id, received_dt=datetime.utcnow())

    db.session.add(heartbeat)
    db.session.commit()

    return heartbeat


def cutoff_year_get(today, month_threshold):
    """
    Work out the cutoff year, where all rows older the cutoff month in that year,
    as well as any rows older than that year, will be deleted.
    """

    return today.year - 1 if (today.month - month_threshold) < 1 else today.year


def cutoff_month_get(today, month_threshold):
    """
    Work out the cutoff month, where all rows older than that month in the cutoff year
    will be deleted.
    """

    return (
        12
        if (today.month - month_threshold) == 0
        else ((today.month - month_threshold) % 12)
    )


def get_last_query():
    """
    Return an SQL query object for finding the last heartbeat for each server.
    If used as a subquery, the last heartbeat can be accessed under the "last_hbeat" field,
    like so:

        subquery.c.last_hbeat

    """

    # TODO: optimise this query so it doesn't take forever
    # e.g. SELECT received_dt FROM heartbeats ORDER BY received_dt DESC LIMIT 1
    return db.session.query(
        Model.server_id, sqlalchemy.func.max(Model.received_dt).label("last_hbeat")
    ).group_by(Model.server_id)


def get_last(server_id=None):
    """
    Get the last heartbeat for each server, or a given server ID.
    """

    if server_id:
        # Slightly optimised query for selecting an individual server ID.
        return (
            db.session.query(Model.received_dt)
            .filter(Model.server_id == server_id)
            .order_by(sqlalchemy.desc(Model.received_dt))
            .first()
        )
    else:
        return dict(get_last_query().all())


def delete_old():
    """
    Delete all rows in the heartbeats table older than 2 calendar months.
    """

    month_threshold = 2

    today = date.today()

    cutoff_dt = datetime(
        cutoff_year_get(today, month_threshold),
        cutoff_month_get(today, month_threshold),
        1,  # day
    )

    for hbeat in db.session.query(Model).filter(Model.received_dt < cutoff_dt).all():
        db.session.delete(hbeat)
    db.session.commit()
