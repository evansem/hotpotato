"""
Sender functions.
"""


import textwrap
import traceback

from flask import current_app

from hotpotato import notifications, oncall_contacts
from hotpotato.models import db
from hotpotato.notifications import exceptions, senders


def using_all(notif):
    """
    Try to send a notification using all available senders, in Hot Potato's preferred order.
    """

    _send(notif, oncall_contacts.get_for_user_id(notif.user_id))


def using_contact_id(notif, contact_id):
    """
    Try to send a notification using a given contact ID.
    """

    try:
        contact = oncall_contacts.get(contact_id)
    except oncall_contacts.OncallContactIDError as err:
        raise exceptions.NotificationSendError(
            "While trying to send {} with ID {} "
            "to contact with ID {}: {} ({})".format(
                notif.description, notif.id, contact_id, str(err), type(err).__name__
            )
        )

    using_contact(notif, contact)


def using_contact(notif, contact):
    """
    Try to send a notification using a given contact.
    """

    _send(notif, tuple((contact,)))


def using_method(notif, method):
    """
    Try to send a notification using a given sending method.
    """

    if method not in notifications.METHOD:
        raise exceptions.NotificationSendError(
            "Invalid or unsupported sending method '{}'".format(method)
        )

    _send(notif, oncall_contacts.get_by(user_id=notif.user_id, method=method))


def using_sender(notif, sender, contact):
    """
    Try to send a notification using a given sender and a given contact.
    Usually only used for testing purposes.
    """

    _send(notif, tuple((contact,)), senders_for_method=lambda m: tuple((sender,)))


# pylint: disable=too-many-statements,too-many-branches,too-many-locals
def _send(notif, contacts, senders_for_method=None):
    """
    Sending helper method that does all the heavy lifting.
    """

    # TODO: add support for tenants?

    if not senders_for_method:
        senders_for_method = senders.for_method

    # Data to update the notification with.
    success = False
    tried_to_send = False
    errors = []
    warnings = []
    method = None
    provider = None
    provider_notif_id = None

    # Sanity checking.
    if notif.is_sending():
        return
    if not notif.user_id:
        notif.json["status"] = "SEND_FAILED"
        notif.json["errors"].append(
            "While sending: Notification with ID {} "
            "has no destination user ID".format(notif.id)
        )
        return

    # Set the notification status to SENDING as early as possible in the process,
    # to put a lock on the notification log entry so that other workers do not try
    # to send this notification, if it is somehow on the queue more than once.
    notif.json["status"] = "SENDING"
    db.session.commit()

    # Catch all exceptions from this point and make sure they get logged into the notification.
    # pylint: disable=too-many-nested-blocks
    try:
        for i, contact in enumerate(contacts):
            # When sending an alert we need to check that the user has a contact method
            # where sending pages has been enabled
            if notif.notif_type == "alert" and not contact.send_pages:
                current_app.logger.debug(
                    "Skipped sending because send_pages is disabled "
                    "for contact with ID {} for user {}".format(
                        contact.id, notif.user_id
                    )
                )
                continue

            if not current_app.config["{}_ENABLED".format(contact.method.upper())]:
                current_app.logger.debug(
                    "Skipped sending because the contact method '{}' is disabled "
                    "for contact with ID {} for user {}".format(
                        contact.method, contact.id, notif.user_id
                    )
                )
                continue

            tried_to_send = True
            data = None

            try:
                # TODO: finish this part (what does this mean again?)
                ss = senders_for_method(contact.method)
                for j, sender in enumerate(ss):

                    try:
                        current_app.logger.debug(
                            "Attempting to send {} using {} "
                            "to contact with number '{}'".format(
                                notif.description, sender.description, contact.contact
                            )
                        )

                        data = sender.send(notif, contact)

                        errors.extend(data["errors"])
                        warnings.extend(data["warnings"])

                        if data["success"]:
                            success = True
                            method = sender.method
                            provider = sender.provider
                            provider_notif_id = data["provider_notif_id"]
                            break  # Operation succeeded, breaks out here to update the object.

                        else:
                            err_message = (
                                "Failed to send using {} " "to contact with number '{}'"
                            ).format(sender.description, contact.contact)
                            errors.append(err_message)
                            current_app.logger.error(err_message)
                            if (j + 1) < len(ss):
                                current_app.logger.error(
                                    "Will now try to send using {} "
                                    "to contact with number '{}'".format(
                                        ss[j + 1].description, contact.contact
                                    )
                                )

                    except Exception as err:
                        err_message = (
                            "Failed to send using {} "
                            "to contact with number '{}': {} ({})"
                        ).format(
                            sender.description,
                            contact.contact,
                            str(err),
                            type(err).__name__,
                        )
                        if current_app.config["DEBUG"] or current_app.config["TESTING"]:
                            for l in traceback.format_tb(err.__traceback__):
                                err_message += "\n{}".format(l)
                        errors.append(err_message)
                        current_app.logger.error(err_message)
                        if (j + 1) < len(ss):
                            current_app.logger.error(
                                "Will now try to send using {} "
                                "to contact with number '{}'".format(
                                    ss[j + 1].description, contact.contact
                                )
                            )

                # If we got to this point, unsuccessful sending via the current method.
                # Try the next one.
                err_message = "Failed to send to contact with number '{}'".format(
                    contact.contact
                )
                errors.append(err_message)
                current_app.logger.exception(err_message)
                if (i + 1) < len(contacts):
                    current_app.logger.error(
                        "Will now try to send to contact with number '{}'".format(
                            contacts[i + 1].contact
                        )
                    )

            # pylint: disable=broad-except
            except Exception as err:
                err_message = "Failed to send to contact with number '{}': {} ({})".format(
                    contact.contact, str(err), type(err).__name__
                )
                if current_app.config["DEBUG"] or current_app.config["TESTING"]:
                    for l in traceback.format_tb(err.__traceback__):
                        err_message += "\n{}".format(l)
                errors.append(err_message)
                current_app.logger.exception(err_message)
                if (i + 1) < len(contacts):
                    current_app.logger.error(
                        "Will now try to send to contact with number '{}'".format(
                            contacts[i + 1].contact
                        )
                    )

        # Update the notification object with the sending status.
        notif.json["status"] = "SENT_TO_PROVIDER" if success else "SEND_FAILED"
        notif.json["errors"] = errors
        notif.json["warnings"] = warnings
        notif.json["method"] = method
        notif.json["provider"] = provider
        notif.json["provider_notif_id"] = provider_notif_id
        db.session.commit()

    # pylint: disable=broad-except
    except Exception as err:
        err_message = "While sending: {} ({})".format(str(err), type(err).__name__)
        if current_app.config["DEBUG"] or current_app.config["TESTING"]:
            for l in traceback.format_tb(err.__traceback__):
                err_message += "\n{}".format(l)
        errors.append(err_message)
        current_app.logger.exception(err_message)

        notif.json["status"] = "SEND_FAILED"
        notif.json["errors"] = errors
        notif.json["warnings"] = warnings
        notif.json["method"] = method
        notif.json["provider"] = provider
        notif.json["provider_notif_id"] = str(
            provider_notif_id
        )  # Double check it's a string.
        db.session.commit()

    if not tried_to_send:
        output = (
            "Unable to find a sending provider that could send the {} "
            "to user with ID {}\n"
            "Please check that the user has on-call contacts configured"
        ).format(notif.description, notif.user_id)
        raise exceptions.NotificationSendError(output)

    # Handle error output if sending failed.
    if not success:
        output = "Failed to send {} with ID {}\n".format(notif.description, notif.id)
        if warnings:
            output += "\nWarnings:\n"
            for warning in warnings:
                output += "*" + textwrap.indent(warning, "  ")[1:]
            output += "\n"
        if errors:
            output += "\nErrors:\n"
            for error in errors:
                output += "*" + textwrap.indent(error, "  ")[1:]
            output += "\n"
        output += "Please check the {}'s log entry for more details".format(
            notif.description
        )
        raise exceptions.NotificationSendError(output)
