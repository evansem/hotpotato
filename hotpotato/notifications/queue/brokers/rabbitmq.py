"""
Dramatiq RabbitMQ message broker.
"""


import dramatiq
import dramatiq.brokers.rabbitmq
import flask
import pika.credentials


def init():
    """
    Initialise the Dramatiq RabbitMQ message broker.
    """

    config = flask.current_app.config

    broker = dramatiq.brokers.rabbitmq.RabbitmqBroker(
        host=config["RABBITMQ_SERVER"],
        port=config["RABBITMQ_PORT"],
        ssl=config["RABBITMQ_USE_SSL"],
        credentials=pika.credentials.PlainCredentials(
            username=config["RABBITMQ_USERNAME"], password=config["RABBITMQ_PASSWORD"]
        ),
        virtual_host=config["RABBITMQ_VHOST"],
    )

    dramatiq.set_broker(broker)
    return broker
