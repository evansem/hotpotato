"""
Dramatiq stub message broker. Useful for unit testing.
"""


import dramatiq
import dramatiq.brokers.stub


def init():
    """
    Initialise the Dramatiq stub message broker.
    """

    broker = dramatiq.brokers.stub.StubBroker()

    dramatiq.set_broker(broker)
    return broker
