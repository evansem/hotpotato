"""
Notification message queue functions.
"""


from hotpotato.notifications.queue.brokers import rabbitmq, stub


def init_app(app):
    """
    Initialise the notification message queue.
    """

    with app.app_context():
        test = False  # TODO: Incorporate FLASK_TESTING into this.
        if test:
            stub.init()
        else:
            rabbitmq.init()
