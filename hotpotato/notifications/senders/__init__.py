"""
Notification sender constants and helper classes.
"""


from flask import current_app

from hotpotato.notifications.senders import (
    pager_modica,
    pushover,
    sms_modica,
    sms_twilio,
)
from hotpotato.notifications.senders.debug_sender import DebugSender

APP = tuple()
# APP = ( # TODO: Hot Potato app
#    app.AppSender(),
# )

PUSHOVER = tuple((pushover.PushoverSender(),))

PAGER = tuple((pager_modica.ModicaPagerSender(),))

SMS = tuple((sms_modica.ModicaSMSSender(), sms_twilio.TwilioSMSSender()))

ALL = APP + PUSHOVER + PAGER + SMS


def for_method(method):
    """
    Return the possible senders for the given method.
    """

    if current_app.config["TESTING"]:
        return tuple((DebugSender(method),))
    else:
        if method == "app":
            return APP
        elif method == "pushover":
            return PUSHOVER
        elif method == "pager":
            return PAGER
        elif method == "sms":
            return SMS
        else:
            raise ValueError(method)
