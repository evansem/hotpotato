#!/usr/bin/env python3
"""The setup script."""

import setuptools

import hotpotato

requirements = [
    # Base Application
    "click~=7.0",
    "click-datetime~=0.2",
    "cockroachdb~=0.2.1",
    "dramatiq[rabbitmq, watch]~=1.3",
    "flask-migrate~=2.2",
    "flask-security~=3.0",
    "flask-sqlalchemy~=2.3",
    "flask~=1.0",
    "Flask-WTF",
    "python-dateutil~=2.7",
    "pytz",
    "requests~=2.20",
    "sqlalchemy~=1.2",
    "sqlalchemy-utils~=0.33",
    "webargs~=4.1",
    "tzlocal",
    "faker",
    "factory_boy~=2.11",
    "werkzeug~=0.14",
    # Twilio
    "twilio~=6.20",
    # Security
    "bcrypt~=3.1",
    # Testing
    "faker~=1.0",
]


packages = setuptools.find_packages(where="./", include=["hotpotato", "hotpotato.*"])
if not packages:
    raise ValueError("No packages detected.")

setuptools.setup(
    name="hotpotato",
    version=hotpotato.__version__,
    description="",
    long_description="",
    author="Hot Potato Team",
    author_email="help@hotpotato.nz",
    url="https://hotpotato.nz",
    packages=packages,
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
)
